# -*- coding: utf-8 -*-
# username: huangqun
"""
步骤二
1.思路中，编写时应先写初始化打开浏览器、输入url，考虑到此操作不在业务逻辑里，
可拆开单独初始化，写入basicset中;为了每个类都能调用到，将basicset作为父类
2.测试用例编写，选执行成功的用例作为调试代码，链式调用完成用例步骤
3.测试用例中完成断言
"""
import time
import allure
import pytest
import logging

from weixintest.POmodel.HomePage import HomePage
from weixintest.testcase.get_data import get_datas

# 执行测试用例中打印日志,pytest.ini必须在测试用例路径下才能产生log文件
logging.basicConfig( level=logging.INFO )
logger = logging.getLogger( name='log.log' )


@pytest.fixture( scope='module' )
def prepare_action():
    print( '开始' )
    home_page = HomePage()
    yield home_page
    print( '结束' )


@allure.feature( '首页-添加成员' )
class TestAddMember():
    get_data_class = get_datas()

    @allure.title( f"保存按钮测试，测试数据：{get_data_class.testdate( 'seccess' )[0]}" )
    @allure.story( "用例1：输入正确，添加成员成功" )
    # 四.4 测试数据放入.yml晚间，BasicSet中读取传入
    @pytest.mark.parametrize( 'name, id, tel' , [get_data_class.testdate( "seccess" )[0]] ,
                              ids=get_data_class.testdate( "title" )[0] )
    def test_add_seccess(self , name , id , tel , prepare_action):
        """
        1.实例化
        2.链式调用方法实现添加成员并保存成功的用例
        3.url初始化，使用浏览器复用机制（卡这了），
        注：新建basicset.py文件，新建类，po建模的类继承该类
        :return:
        """
        # 该模块都是由首页发起执行，实例化统一实现，使用fixture写入conftest.py
        # home_page = HomePage()
        # home_page.add_member_button().fill_in_member_information(name, id, tel).get_member_information()
        print( self.get_data_class.testdate( "seccess" )[0] )
        # 此处应有断言
        with allure.step( f'断言，{name}在通讯录内' ):
            assert name in prepare_action.add_member_button() \
                .fill_in_member_information( name , id , tel ).get_member_information()
            time.sleep( 2 )
        prepare_action.turnback_homepage()
        logger.info( f"用例1执行完毕，执行参数{self.get_data_class.testdate( 'seccess' )[0]}" )
        allure.attach.file( '../photo/plantuml_PO.jpg' , name='uml时序图' , attachment_type=allure.attachment_type.JPG )
        allure.attach.file( '../photo/用例简写.jpg' , name='xmind用例' , attachment_type=allure.attachment_type.JPG )
        allure.attach.file( '../photo/test_report.jpg' , name='测试报告' , attachment_type=allure.attachment_type.JPG )

    @allure.title( f'"姓名"字段必录校验,测试数据：{get_data_class.testdate( "fail" )[0]}' )
    @allure.story( "用例2：姓名为空，添加成员失败" )
    @pytest.mark.parametrize( 'name, id, tel' , [get_data_class.testdate( "fail" )[0]] ,
                              ids=get_data_class.testdate( "title" )[1] )
    def test_account_none_fail(self , name , id , tel , prepare_action):
        print( self.get_data_class.testdate( "fail" )[0] )
        with allure.step( f'断言，{name}在通讯录内' ):
            assert name not in prepare_action.add_member_button() \
                .fill_in_member_information( name , id , tel ).get_member_information()
            time.sleep( 2 )
        prepare_action.turnback_homepage()
        logger.info( f"用例2执行完毕，执行参数{self.get_data_class.testdate( 'fail' )[0]}" )

    @allure.title( f'“账号”字段必录校验,测试数据：{get_data_class.testdate( "fail" )[1]}' )
    @allure.story( "用例3：账号为空，添加成员失败" )
    @pytest.mark.parametrize( 'name, id, tel' , [get_data_class.testdate( "fail" )[1]] ,
                              ids=get_data_class.testdate( "title" )[2] )
    def test_account_none_fail(self , name , id , tel , prepare_action):
        print( self.get_data_class.testdate( "fail" )[1] )
        with allure.step( f'断言，{name}不在通讯录内' ):
            assert name not in prepare_action.add_member_button() \
                .fill_in_member_information( name , id , tel ).get_member_information()
            time.sleep( 2 )
        prepare_action.turnback_homepage()
        logger.info( f"用例3执行完毕，执行参数{self.get_data_class.testdate( 'fail' )[1]}" )

    @allure.title( f'账号重复校验,测试数据：{get_data_class.testdate( "fail" )[2]}' )
    @allure.story( "用例4：账号重复，添加成员失败" )
    @pytest.mark.parametrize( 'name, id, tel' , [get_data_class.testdate( "fail" )[2]] ,
                              ids=get_data_class.testdate( "title" )[3] )
    def test_exist_account_id_fail(self , name , id , tel , prepare_action):
        print( self.get_data_class.testdate( "fail" )[2] )
        with allure.step( f'断言，{name}不在通讯录内' ):
            assert name not in prepare_action.add_member_button() \
                .fill_in_member_information( name , id , tel ).get_member_information()
            time.sleep( 2 )
        prepare_action.turnback_homepage()
        logger.info( f"用例4执行完毕，执行参数{self.get_data_class.testdate( 'fail' )[2]}" )

    @allure.title( f'手机号重复校验,测试数据：{get_data_class.testdate( "fail" )[3]}' )
    @allure.story( "用例5：手机号重复，添加成员失败" )
    @pytest.mark.parametrize( 'name, id, tel' , [get_data_class.testdate( "fail" )[3]] ,
                              ids=get_data_class.testdate( "title" )[4] )
    def test_name_none_fail(self , name , id , tel , prepare_action):
        print( self.get_data_class.testdate( "fail" )[3] )
        with allure.step( f'断言，{name}不在通讯录内' ):
            assert name not in prepare_action.add_member_button() \
                .fill_in_member_information( name , id , tel ).get_member_information()
            time.sleep( 2 )
        prepare_action.turnback_homepage()
        logger.info( f"用例5执行完毕，执行参数{self.get_data_class.testdate( 'fail' )[3]}" )

    @allure.title( f'手机号格式校验,测试数据：{get_data_class.testdate( "fail" )[4]}' )
    @allure.story( "用例6：手机号格式不对，添加成员失败" )
    @pytest.mark.parametrize( 'name, id, tel' , [get_data_class.testdate( "fail" )[4]] ,
                              ids=get_data_class.testdate( "title" )[5] )
    def test_phone_num_form_fail(self , name , id , tel , prepare_action):
        print( self.get_data_class.testdate( "fail" )[4] )
        with allure.step( f'断言，{name}不在通讯录内' ):
            assert name not in prepare_action.add_member_button() \
                .fill_in_member_information( name , id , tel ).get_member_information()
            time.sleep( 2 )
        prepare_action.turnback_homepage()
        logger.info( f"用例6执行完毕，执行参数{self.get_data_class.testdate( 'fail' )[4]}" )

    @allure.title( f'手机号为空校验,测试数据：{get_data_class.testdate( "fail" )[5]}' )
    @allure.story( "用例7：手机号为空，添加成员失败" )
    @pytest.mark.parametrize( 'name, id, tel' , [get_data_class.testdate( "fail" )[5]] ,
                              ids=get_data_class.testdate( "title" )[6] )
    def test_phone_none_fail(self , name , id , tel , prepare_action):
        print( self.get_data_class.testdate( "fail" )[5] )
        with allure.step( f'断言，{name}不在通讯录内' ):
            assert name not in prepare_action.add_member_button() \
                .fill_in_member_information( name , id , tel ).get_member_information()
            time.sleep( 2 )
        prepare_action.turnback_homepage()
        logger.info( f"用例7执行完毕，执行参数{self.get_data_class.testdate( 'fail' )[5]}" )
