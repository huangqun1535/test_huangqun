# -*- coding: utf-8 -*-
# username: huangqun
import pytest

from weixintest.POmodel.HomePage import HomePage


def pytest_collection_modifyitems(session , config , items):
    for item in items:
        # item.name 用例名字
        item.name = item.name.encode( 'utf-8' ).decode( 'unicode-escape' )
        print( item.nodeid )
        # item.nodeid 用例路径
        item._nodeid = item.nodeid.encode( "utf-8" ).decode( 'unicode_escape' )

# 这么写所有class都初始化了HomePage()，用scope="module"写到测试用例里
# @pytest.fixture(scope='class')
# def prepare_action():
#     print('开始')
#     home_page = HomePage()
#     yield home_page
#     print('结束')


# @pytest.fixture()
# def basepage():
#     print( '开始执行用例' )
#     yield
#     print( '用例执行结束' )
