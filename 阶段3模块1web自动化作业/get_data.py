# -*- coding: utf-8 -*-
# username: huangqun
import yaml


class get_datas:

    def testdate(self , sign):
        with open( '../datas/testdatas.yml' , encoding='utf-8' ) as f:
            data = yaml.safe_load( f )
            values = data[sign]
            return values


def get_cookies():
    get_cookie = [{'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.vid', 'path': '/', 'secure': False, 'value': '1688850260931747'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.vst', 'path': '/', 'secure': False, 'value': 'FaGlBL5ZHnPHumQP233mfXNsFjReeAd4zStshNSeK4WvKZIT9uAQxsZ-rMFraelVkGsJfqQMAATzRBUEXjQ_a05-qzCbfztDFn4MwgZG2SI-pMsRE07YMn1ouUlVI8pTnAj9fh33wjJcNR3XYimcS3OBjIiGsAf-GmRgDBkHGf_Y13FyQ0HnEfwtdOsUFdHNP4hi1glyUKl_oiEVz60Uo3DweJ2nGh_Hlh-hTpwX4T0RbRmq3Py5wyd7X_SaUx6N4_-nfhMJTfM5FBfByr76zA'}, {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.vid', 'path': '/', 'secure': False, 'value': '1688850260931747'}, {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.corpid', 'path': '/', 'secure': False, 'value': '1970325019462757'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ref', 'path': '/', 'secure': False, 'value': 'direct'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ltype', 'path': '/', 'secure': False, 'value': '1'}, {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.d2st', 'path': '/', 'secure': False, 'value': 'a9148616'}, {'domain': '.qq.com', 'expiry': 1626108323, 'httpOnly': False, 'name': '_gid', 'path': '/', 'secure': False, 'value': 'GA1.2.756599326.1626021910'}, {'domain': 'work.weixin.qq.com', 'expiry': 1626053447, 'httpOnly': True, 'name': 'ww_rtkey', 'path': '/', 'secure': False, 'value': '2jhc8qc'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.refid', 'path': '/', 'secure': False, 'value': '17621935912708638'}, {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.cs_ind', 'path': '/', 'secure': False, 'value': ''}, {'domain': '.qq.com', 'expiry': 1626021970, 'httpOnly': False, 'name': '_gat', 'path': '/', 'secure': False, 'value': '1'}, {'domain': '.work.weixin.qq.com', 'expiry': 1657557908, 'httpOnly': False, 'name': 'wwrtx.c_gdpr', 'path': '/', 'secure': False, 'value': '0'}, {'domain': '.qq.com', 'expiry': 1689093923, 'httpOnly': False, 'name': '_ga', 'path': '/', 'secure': False, 'value': 'GA1.2.178072952.1626021910'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.sid', 'path': '/', 'secure': False, 'value': 'LAo1wDAGySzqOu1IOslU6ZE-yxXPeMeIEyy0cec4qquL7U_-qoACubLLj3j5BKfW'}, {'domain': '.work.weixin.qq.com', 'expiry': 1628613925, 'httpOnly': False, 'name': 'wwrtx.i18n_lan', 'path': '/', 'secure': False, 'value': 'zh'}]
    return get_cookie

# if __name__ == '__main__':
#     a = get_datas()
#     print( a.testdate( "fail" ) )
