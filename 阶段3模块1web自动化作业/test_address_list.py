# -*- coding: utf-8 -*-
# username: huangqun
import time
import allure
import pytest
import logging

from weixintest.POmodel.address_list_page import AddressList

logging.basicConfig( level=logging.INFO )
logger = logging.getLogger( name='log.log' )

from weixintest.testcase.get_data import get_datas


@pytest.fixture( scope='module' )
def addresslist_action():
    print( '开始' )
    addresslist_page = AddressList()
    yield addresslist_page
    print( '结束' )


# 通讯录界面执行click()没有跳转。。。。没找到原因
@allure.feature( '通讯录-添加成员' )
class TestAddressListAddMember():
    # 点击无效试试ActionChains(self.driver),basicpage已解决w3c错误，但是界面按钮看到点击了，页面没有跳转，放弃=。=

    get_data_class = get_datas()

    @allure.title( f"保存按钮测试，测试数据：{get_data_class.testdate( 'seccess' )[0]}" )
    @allure.story( "用例1：输入正确，添加成员成功" )
    # 四.4 测试数据放入.yml晚间，BasicSet中读取传入
    @pytest.mark.parametrize( 'name, id, tel' , [get_data_class.testdate( "seccess" )[0]] ,
                              ids=get_data_class.testdate( "title" )[0] )
    def test_add_seccess(self , name , id , tel , addresslist_action):
        print( self.get_data_class.testdate( "seccess" )[0] )
        # 此处应有断言
        with allure.step( f'断言，{name}在通讯录内' ):
            assert name in addresslist_action.add_member_button() \
                .fill_in_member_information( name , id , tel ).get_member_information()
            time.sleep( 2 )
        addresslist_action.turnback_homepage()
        logger.info( f"用例1执行完毕，执行参数{self.get_data_class.testdate( 'seccess' )[0]}" )
