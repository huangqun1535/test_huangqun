# -*- coding: utf-8 -*-
# username: huangqun
import json

import mitmproxy
from mitmproxy import http , ctx
from mitmproxy.tools._main import mitmdump

# mitmdump实现mapLocal
class Events:

    def request(self, flow: mitmproxy.http.HTTPFlow):
        # 判断准入条件，篡改url中存在quote.js的请求，报错Addon error: Traceback (most recent call last)
        # url = "https://stock.xueqiu.com/v5/stock/batch/quote.json"
        # if flow.request.url.startswith(url):
        if "quote.json" in flow.request.pretty_url and \
                "x=" in flow.request.pretty_url:            # 打开模拟服务器响应的响应文件
            # mitmproxy请求内容缺失(Request content missing)返回413状态码
            # 启动时：mitmdump.exe --set http2=false
            with open('../files/quote.json', encoding="utf-8") as f:
                # make一个响应，相应为读取f文件的内容
                flow.response = http.HTTPResponse.make(
                    200,
                    f.read(),
                    {"content-type": "application/json"}
                )

#=================================================================================

    ## mitmdump实现rewrite
    def respones(self, flow:http.HTTPFlow):
        # #找不到请求
        # url="https://stock.xueqiu.com/v5/stock/batch/quote.json"
        # if flow.request.url.startswith(url):
            if "quote.json" in flow.request.pretty_url and "x=" in flow.request.pretty_url:
                # 把响应数据转化成python对象，保存到datas中
                datas = json.loads(flow.response.content)
                # 获取请求中的content
                #ctx.log.info(str(flow.response.content))
                # 篡改第一组数据的股票名称
                datas["data"]["items"][0]["quote"]["name"] = "rewrite" + datas["data"]["items"][0]["quote"]["name"]
                # 篡改第一组数据的股票现价
                datas["data"]["items"][0]["quote"]["current"] = "666.666"
                # 篡改第一组数据的股票涨跌幅
                datas["data"]["items"][0]["quote"]["percent"] = "0"
                # 篡改第二组数据的股票名称
                datas["data"]["items"][1]["quote"]["name"]="rewrite" + datas["data"]["items"][1]["quote"]["name"]
                # 篡改第二组数据的股票现价
                datas["data"]["items"][1]["quote"]["current"]="8888"
                # 篡改第二组数据的股票涨跌幅
                datas["data"]["items"][1]["quote"]["percent"]="-1.5"
                # 篡改第三组数据的股票名称
                datas["data"]["items"][2]["quote"]["name"]="rewrite"+datas["data"]["items"][2]["quote"]["name"]
                # 篡改第三组数据的股票现价
                datas["data"]["items"][2]["quote"]["current"]="2333"
                # 篡改第三组数据的股票涨跌幅
                datas["data"]["items"][2]["quote"]["percent"]="1.5"
                # 把修改后的内容赋值给 response 原始数据格式
                flow.response.text = json.dumps(datas)


# 实例化请求
addons = [
    Events()
]

if __name__ == '__main__':
    mitmdump(["-s", "mock_HttpEvent.py", "-p", "8080"])