# -*- coding: utf-8 -*-
# username: huangqun
from appium.webdriver.common.mobileby import MobileBy

from AppiumPOModul.basepage import BasePage


class EditMemberEntryPage( BasePage ):
    """
    1.1 点击编辑成员按钮，转到编辑成员界面
    """

    _edit_button=(MobileBy.ID , "com.tencent.wework:id/azd")

    # def __init__(self, driver:WebDriver):
    #     self.driver = driver

    def edit_member_button(self):
        from AppiumPOModul.EditMemberPage import EditMemberPage
        self.click( *self._edit_button )

        return EditMemberPage( self.driver )
