# -*- coding: utf-8 -*-
# username: huangqun
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from AppiumPOModul.ContactsPage import ContactsPage
from AppiumPOModul.basepage import BasePage


class HomePage( BasePage ):
    _Contacts=(MobileBy.XPATH , "//*[@resource-id='com.tencent.wework:id/gfj']/android.widget.RelativeLayout[2]")

    # 填充内容时发现driver不可用，构造一个driver后再接AppCtrl的driver
    # def __init__(self, driver:WebDriver):
    #     self.driver = driver
    """
    1.1 首页点击通讯录页签
    """

    def goto_ContactsPage(self):
        self.click( *self._Contacts )
        return ContactsPage( self.driver )
