# -*- coding: utf-8 -*-
# username: huangqun
from appium.webdriver.common.mobileby import MobileBy
from selenium.common.exceptions import NoSuchElementException
from AppiumPOModul.AddMemberPage import AddMemberPage
from AppiumPOModul.PersonalInformationPage import PersonalInformationPage
from AppiumPOModul.SearchPage import SearchPage
from AppiumPOModul.basepage import BasePage


class ContactsPage( BasePage ):
    """
    1.1 点击添加成员，滑动定位元素，转到添加成员页面
    1.2 点击搜索按钮，转到搜索页面
    """
    _add_button=(MobileBy.XPATH , "//*[@resource-id='com.tencent.wework:id/h14']/*[@text='添加成员']")
    _search_button=(MobileBy.ID , "com.tencent.wework:id/guu")
    _delele_firstone=(
    MobileBy.XPATH , "//*[@resource-id='com.tencent.wework:id/b00']/*[@index='1']//*[@class='android.view.ViewGroup']")

    # def __init__(self, driver:WebDriver):
    #     self.driver = driver

    def goto_AddMemberPage(self , num=5):

        for i in range( num ):
            if i == num-1:
                raise NoSuchElementException( f"找了{num}次，未找到按钮，请增加滑动次数" )
            ele=self.driver.find_elements( *self._add_button )
            if len( ele ) == 0:
                print( len( ele ) )
                print( f"第{i}次滑动没有找到'添加成员'按钮" )

                size=self.driver.get_window_size()
                width=size.get( "width" )
                height=size.get( "height" )
                start_x=width/2
                start_y=height*0.8
                end_x=start_x
                end_y=height*0.2
                self.driver.swipe( start_x , start_y , end_x , end_y , 1000 )
            else:
                print( f"第{i}次滑动可以点击'添加成员'按钮" )
                self.click( *self._add_button )
                break
        return AddMemberPage( self.driver )

    def goto_searchpage(self):
        self.click( *self._search_button )
        return SearchPage( self.driver )

    def clear_member_list(self):
        self.click( *self._delele_firstone )
        return PersonalInformationPage( self.driver )
