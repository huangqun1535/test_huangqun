# -*- coding: utf-8 -*-
# username: huangqun
import time

from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from AppiumPOModul.basepage import BasePage


class FillInMemberInformationPage( BasePage ):
    """
    填写成员信息
    1.1 填写地址选项，转到编辑地址页面
    1.2 点击保存，获取toast，转到添加成员页面
    """
    # def __init__(self, driver:WebDriver):
    #     self.driver = driver

    _name_ele=(MobileBy.XPATH , "//*[@resource-id='com.tencent.wework:id/dvd']//*[@text='必填']")
    _phone_ele=(MobileBy.ID , "com.tencent.wework:id/eq7")
    _address_ele=(MobileBy.XPATH , "//*[@resource-id='com.tencent.wework:id/duf']//*[@text='选填']")
    _save_button=(MobileBy.ID , "com.tencent.wework:id/gur")
    _toast_ele=(MobileBy.XPATH , "//*[@class='android.widget.Toast']")

    def fill_in_member_information(self , name , phone):
        from AppiumPOModul.EditAddressPage import EditAddressPage
        self.driver.find_element( *self._name_ele ).send_keys( name )
        self.driver.find_element( *self._phone_ele ).send_keys( phone )
        self.click( *self._address_ele )  # 与地址编辑页面的ID一样，地址编辑页面没有点击会导致保存按钮不执行点击

        return EditAddressPage( self.driver )

    def save_button(self):
        from AppiumPOModul.AddMemberPage import AddMemberPage
        WebDriverWait( self.driver , 10 ).until( expected_conditions.element_to_be_clickable( self._save_button ) )
        self.click( *self._save_button )
        time.sleep( 3 )
        # print(self.driver.page_source)
        save_tip=self.driver.find_element( *self._toast_ele ).get_attribute( "text" )
        # print(tip)
        return AddMemberPage( self.driver ) , save_tip
