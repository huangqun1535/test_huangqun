# -*- coding: utf-8 -*-
# username: huangqun
import time

from appium.webdriver.common.mobileby import MobileBy
from AppiumPOModul.SearchPage import SearchPage
from AppiumPOModul.basepage import BasePage


class DeleteOptionsPage( BasePage ):
    """
    1.1 确认删除，转到搜索界面
    1.2 取消删除，留在编辑成员界面
    """

    _confirm_button=(MobileBy.ID , "com.tencent.wework:id/b_4")
    _cancel_delete=(MobileBy.ID , "com.tencent.wework:id/b_2")
    _toast_ele=(MobileBy.XPATH , "//*[@class='android.widget.Toast']")

    # def __init__(self, driver: WebDriver):
    #     self.driver = driver

    def confirm_delete(self):
        self.click( *self._confirm_button )
        # time.sleep(3)
        # print(self.driver.page_source)
        # delete_tip = self.driver.find_element(*self._toast_ele).get_attribute("text")
        # print(delete_tip)
        return SearchPage( self.driver )

    def cancel_delete(self):
        from AppiumPOModul.EditMemberPage import EditMemberPage
        self.click( *self._cancel_delete )

        return EditMemberPage( self.driver )
