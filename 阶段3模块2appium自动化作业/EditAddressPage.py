# -*- coding: utf-8 -*-
# username: huangqun
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from AppiumPOModul.FillInMemberInformationPage import FillInMemberInformationPage
from AppiumPOModul.basepage import BasePage


class EditAddressPage( BasePage ):
    """
    1.1 填写地址，点击确定按钮
    """
    _edit_addr=(MobileBy.ID , "com.tencent.wework:id/gz")
    _confirm_button=(MobileBy.ID , "com.tencent.wework:id/gur")

    # def __init__(self, driver:WebDriver):
    #     self.driver = driver

    def edit_address(self , address):
        self.driver.find_element( *self._edit_addr ).send_keys( address )
        self.click( *self._confirm_button )

        return FillInMemberInformationPage( self.driver )
