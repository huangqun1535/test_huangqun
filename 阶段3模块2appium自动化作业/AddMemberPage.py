# -*- coding: utf-8 -*-
# username: huangqun
from appium.webdriver.common.mobileby import MobileBy
from AppiumPOModul.FillInMemberInformationPage import FillInMemberInformationPage
from AppiumPOModul.basepage import BasePage


class AddMemberPage( BasePage ):
    # def __init__(self, driver:WebDriver):
    #     self.driver = driver
    """
    1.1点击手动输入添加（add_manually）
    """
    _add_button=(MobileBy.XPATH , "//*[@text='手动输入添加']")

    def add_manually(self):
        self.click( *self._add_button )

        return FillInMemberInformationPage( self.driver )

    # def turn_back(self):
    #     from AppiumPOModul.ContactsPage import ContactsPage
    #     self.driver.back()

    # return ContactsPage(self.driver)
