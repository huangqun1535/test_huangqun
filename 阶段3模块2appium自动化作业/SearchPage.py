# -*- coding: utf-8 -*-
# username: huangqun
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from AppiumPOModul.PersonalInformationPage import PersonalInformationPage
from AppiumPOModul.basepage import BasePage


class SearchPage( BasePage ):
    """
    1.1 查找成员，并收集成员列表，用于断言
    1.2 点击搜索结果，转到个人信息界面
    """
    _search_ele=(MobileBy.ID , "com.tencent.wework:id/fk1")

    # def __init__(self, driver:WebDriver):
    #     self.driver = driver

    def search_and_collect_member(self , name):
        result_eles=(MobileBy.XPATH , f"//*[@resource-id='com.tencent.wework:id/flh']//*[@text='{name}']")
        self.driver.find_element( *self._search_ele ).send_keys( name )
        eles=self.driver.find_elements( *result_eles )
        # print(eles)
        # print(len(eles))
        # 增加判断，如果是给了删除标志，则走删除流程。。。存数之后还是得重新跑
        # if delete_flag == True:
        #     delete_ele = (MobileBy.XPATH , f"//*[@resource-id='com.tencent.wework:id/flh']/*[@index='1']//*[@text='{name}']")
        #     self.driver.find_element(*delete_ele).click()

        return len( eles )

    # 分开两个方法，执行时需要重复操作，步骤不顺畅，合并到上面一条...
    # index提出来做变量，可以选择删第几个搜索结果
    def goto_personal_information_page(self , name , index):
        delete_ele=(
        MobileBy.XPATH , f"//*[@resource-id='com.tencent.wework:id/flh']/*[@index='{index}']//*[@text='{name}']")
        self.driver.find_element( *self._search_ele ).send_keys( name )
        WebDriverWait( self.driver , 10 ).until( expected_conditions.element_to_be_clickable( delete_ele ) )
        self.click( *delete_ele )

        return PersonalInformationPage( self.driver )
