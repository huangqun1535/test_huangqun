# -*- coding: utf-8 -*-
# username: huangqun

# po框架中重复的内容进行封装:__init__,find_element,click,send_keys
import logging

from appium.webdriver.webdriver import WebDriver

logger=logging.getLogger()


class BasePage:

    def __init__(self , driver: WebDriver = None):
        self.driver=driver

    def find(self , by , value):
        logger.info( f"find element:{by},{value}" )
        self.driver.find_element( by , value )

    def click(self , by , value):
        logger.info( f"click element:{by},{value}" )
        self.driver.find_element( by , value ).click()

