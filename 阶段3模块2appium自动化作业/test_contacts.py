# -*- coding: utf-8 -*-
# username: huangqun

# po的架子搭好了，写用例时有点犹豫怎么串起来，思路有点卡，回看app和base_page建立思路
# web与app的操作差异，建立AppCtrl.py文件存放APP特有操作
# 录播的实例化有点绕
# import sys
# sys.path.append("")
import allure
import pytest
from faker import Faker
from AppiumPOModul.AppCtrl import AppCtrl


@allure.feature( "企业微信通讯录添加及删除成员测试用例" )
class TestContacts():

    def setup_class(self):
        # driver初始化，app启动操作
        self.app=AppCtrl()  # 实例化每个类做一次就行，转class级别
        self.faker=Faker( "zh_CN" )  # 全局设置

    def setup(self):
        # driver初始化，app启动操作
        # self.app = AppCtrl() # 实例化每个类做一次就行，转class级别
        # self.main = self.app.start_app() # 这是啥。。。没转过弯来
        self.app.start_app()
        self.name=self.faker.name()
        self.phone=self.faker.phone_number()
        self.address=self.faker.address()
        # self.faker = Faker("zh_CN") #全局设置

    def teardown(self):  # m每条测试用例执行完后回到手机初始页
        self.app.back()

    def teardown_class(self):
        self.app.close_app()  # 测试用例都跑完后再做销毁，转class级别

    @allure.story( "企业微信添加成员测试用例" )
    @allure.title( f"用例1：成功添加成员" )
    def test_add_member(self):
        with allure.step( "step1 开始添加成员" ):
            result=self.app.goto_homepage().goto_ContactsPage().goto_AddMemberPage().add_manually(). \
                fill_in_member_information( self.name , self.phone ).edit_address( self.address ).save_button()[1]
        print( result )
        with allure.step( "step2 断言：点击保存，提示'添加成功'" ):
            assert "添加成功" in result
        allure.attach.file( "../photo/appium测试报告.jpg" , name="appium测试报告" , attachment_type=allure.attachment_type.JPG )
        allure.attach.file( "../photo/appium测试报告.jpg" , name="appium时序图" , attachment_type=allure.attachment_type.JPG )

    @allure.story( "企业微信删除成员测试用例" )
    @allure.title( f"用例2：删除成员成功" )
    def test_delete_member(self):
        name="张三"
        # phone=self.faker.phone_number()
        # address=self.faker.address()
        # 前置：增加删除的账户
        with allure.step( "step1 前置步骤：增加需要删除的成员" ):
            self.app.goto_homepage().goto_ContactsPage().goto_AddMemberPage(). \
                add_manually().fill_in_member_information( name , self.phone ).edit_address(
                self.address ).save_button()
        self.app.turn_back()
        # 删除前查询账户存在个数，重复操作较多，目前的只是无法做优化
        with allure.step( "step2 记录删除前的成员数量" ):
            before_delete=self.app.goto_homepage().goto_ContactsPage().goto_searchpage().search_and_collect_member(
                name )
        self.app.turn_back()
        # 执行删除操作
        with allure.step( "step3 执行删除" ):
            self.app.goto_homepage().goto_ContactsPage().goto_searchpage().goto_personal_information_page( name ,
                                                                                                           index=1 ) \
                .goto_edit_member_entry_page().edit_member_button().delete_member().confirm_delete()
        self.app.turn_back()
        # 删除后查询账户存在个数
        with allure.step( "step4 记录删除后的成员数量" ):
            after_delete=self.app.goto_homepage().goto_ContactsPage().goto_searchpage().search_and_collect_member(
                name )
        # 断言删除前比删除后多一个
        with allure.step( "step5 断言：删除前比删除后多一个" ):
            assert after_delete+1 == before_delete
        print( "删除成功" )

    # def test_delete_the_firstone(self):
    #     # 删除前五个成员
    #     for i in range(5):
    #         self.app.goto_homepage().goto_ContactsPage().clear_member_list().goto_edit_member_entry_page().\
    #             edit_member_button().delete_member().confirm_delete()
