# -*- coding: utf-8 -*-
# username: huangqun
from appium.webdriver.common.mobileby import MobileBy

from AppiumPOModul.basepage import BasePage


class PersonalInformationPage( BasePage ):
    """
    1.1 点击更多按钮，转到编辑成员入口界面
    """
    _more_button=(MobileBy.ID , "com.tencent.wework:id/guk")

    # def __init__(self, driver:WebDriver):
    #     self.driver = driver

    def goto_edit_member_entry_page(self):
        from AppiumPOModul.EditMemberEntryPage import EditMemberEntryPage
        self.click( *self._more_button )

        return EditMemberEntryPage( self.driver )
