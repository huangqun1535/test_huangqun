# -*- coding: utf-8 -*-
# username: huangqun
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from AppiumPOModul.basepage import BasePage


class EditMemberPage( BasePage ):
    """
    1.1 点击删除成员按钮，转到删除选项界面
    """
    _delete_button=(MobileBy.ID , "com.tencent.wework:id/duq")

    # def __init__(self, driver:WebDriver):
    #     self.driver = driver

    def delete_member(self):
        from AppiumPOModul.DeleteOptionsPage import DeleteOptionsPage
        self.click( *self._delete_button )
        return DeleteOptionsPage( self.driver )
