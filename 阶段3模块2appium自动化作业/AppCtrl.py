# -*- coding: utf-8 -*-
# username: huangqun
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from AppiumPOModul.HomePage import HomePage
from AppiumPOModul.basepage import BasePage


class AppCtrl( BasePage ):
    _back=(MobileBy.ID , "com.tencent.wework:id/gu_")

    def start_app(self):
        if self.driver == None:
            print( "driver is none" )
            desir_capability={
                "platformName": "android" ,
                "deviceName": "127.0.0.1:7555" ,
                "appPackage": "com.tencent.wework" ,
                "appActivity": ".launch.LaunchSplashActivity" ,
                "noReset": "True"
            }
            self.driver=webdriver.Remote( "http://127.0.0.1:4723/wd/hub" , desir_capability )
            self.driver.implicitly_wait( 10 )
        else:
            # 启动页面
            print( "driver is not none" )
            self.driver.launch_app()

    def restart_app(self):
        pass

    def close_app(self):
        self.driver.quit()

    def back(self , num=5):
        for i in range( num ):
            self.driver.back()

    # 自带的返回键怎么不管用。。。self.driver.back()
    def turn_back(self):
        self.driver.find_element( *self._back ).click()

    # 传说中的入口
    def goto_homepage(self):
        return HomePage( self.driver )
